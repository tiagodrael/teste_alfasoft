<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //\App\Models\User::factory(1)->create();
        \DB::table('users')->insert(
            [
                'name' => 'Administrador',
                'email' => 'admin@admin.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' =>'password',
            ]
        );
        \DB::table('person')->insert(
            [
                'name' => 'pessoa 1',
                'email' => 'pessoa1@admin.com',
            ],
        );
        \DB::table('person')->insert(
            [
                'name' => 'pessoa 2',
                'email' => 'pessoa2@admin.com',
            ]
        );
    }
}

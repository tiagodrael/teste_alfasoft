@extends('layouts.master')
@section('content')

    <div class="col-sm-12 content">
        <div class="bg-white">
            <div class="col-sm-12">
                <table class="table">
{{--                    {{ dd($countries[0]) }}--}}
                    <form action="{{ route('home.store') }}" method="POST">
                        @csrf
                        <h2>Adicionar Contato</h2>

                        <div class="form-group">
                            <th class="py-2 form-group">
                                <label for="name">Nome</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Digite o nome">
                            </th>
                            <th class="py-2 form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control" placeholder="Digite o email">
                            </th>
                            <th class="py-2 form-group">
                                <label for="single">Contatos</label><br />
                                <select id="single" class="js-states form-control">
                                    @foreach($countries as $contry)
                                        <option value="{{ $contry['name'] }} "> {{ $contry['name'] .' ('. $contry['callingCodes'][0] .')' }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <img src="" alt="" style="width: 100px;height: 100px;">
                            </th>
                        </div>
                        <tr>
                            <td>
                                <a href="{{ route('home.index') }}" class="btn btn-outline-primary">Voltar</a>
                                <button href="" class="btn btn-outline-success">Salvar</button>
                            </td>
                        </tr>
                    </form>
                </table>
            </div>
        </div>
    </div>
@endsection

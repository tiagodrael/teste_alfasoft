@extends('layouts.master')
@section('content')
    <div class="col-sm-12 content">
        <div class="bg-white">
            <div class="col-sm-12">
                <table class="table">
                    <form action="{{ route('home.update', $person->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <h2>Editar Contato</h2>

                        <div class="form-group">
                            <th class="py-2 form-group">
                                <label for="name">Nome</label>
                                <input type="text" name="name" id="name" value="{{ $person->name }}" class="form-control" placeholder="Digite o nome">
                            </th>
                            <th class="py-2 form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" value="{{ $person->email }}" class="form-control" placeholder="Digite o email">
                            </th>
{{--                            <th class="py-2 form-group">--}}
{{--                                <label for="name">Contato</label>--}}
{{--                                <input type="text" name="contato" id="contato" value="123123" class="form-control" placeholder="Digite o Contato">--}}
{{--                            </th>--}}
                            <th>
                                <img src="" alt="" style="width: 100px;height: 100px;">
                            </th>
                        </div>
                        <tr>
                            <td>
                                <a href="{{ route('home.index') }}" class="btn btn-outline-primary">Voltar</a>
                                <button type="submit" class="btn btn-outline-success">Editar</button>
                            </td>
                        </tr>
                    </form>
                </table>
            </div>
        </div>
    </div>
@endsection

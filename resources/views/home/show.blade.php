@extends('layouts.master')
@section('content')
    <div class="col-sm-12 content">
        <div class="bg-white">
            <div class="col-sm-12">
                <table class="table">
                    <tr>
                        <td>
                            <a href="{{ route('home.index') }}" class="btn btn-outline-primary">Voltar</a>
                            @auth
                                <a href="{{ route('home.create') }}" class="btn btn-outline-primary">Adicionar Contato</a>
                            @endauth
                        </td>
                    </tr>
                    <tr>
                        <th class="py-2">Nome</th>
                        <td>{{ $person->name }}</td>
                    </tr>
                    <tr>
                        <th class="py-2">Email</th>
                        <td>{{ $person->email }}</td>
                    </tr>
                    <tr>
                        <th class="py-2">Contato</th>
                        <td>123123</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

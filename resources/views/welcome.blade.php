@extends('layouts.master')
@section('content')
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 sm:items-center py-4 sm:pt-0">

    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ route('dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Painel</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Entrar</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Registrar</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="col-sm-12">
        <div class="bg-white">


            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif


            <div class="col-sm-12">
                @auth
                    <div class="form-group">
                        <a href="{{ route('home.create') }}" class="btn btn-outline-success">Criar Pessoas</a>
                        <a href="" class="btn btn-outline-success">Criar contato</a>
                    </div>
                @endauth
                <table class="table">
                    <thead class="thead-white">
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col-3">Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($people as $person)
                            <tr>
                                <td>{{ $person->name }}</td>
                                <td>{{ $person->email }}</td>
                                <td>
                                    <a href="{{ route('home.show', $person->id) }}" class="btn btn-outline-primary" >Visualizar</a>
                                    @auth
                                        <a href="{{ route('home.edit', $person->id) }}" class="btn btn-outline-success" >Editar</a>
                                        <form action="{{ route('home.destroy',$person->id ) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Tem certeza que deseja excluir?')"  class="btn btn-outline-danger" >Excluir</button>
                                        </form>
                                    @endauth
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
